from cProfile import label
import torch
import matplotlib.pyplot as plt
import unittest


class ActivationTestCase(unittest.TestCase):
    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)

    def setUp(self) -> None:
        return super().setUp()

    def TestELU(self):
        """
        Applies the Exponential Linear Unit (ELU) function, element-wise,
        as described in the paper: Fast and Accurate Deep Network Learning by Exponential Linear Units (ELUs).
        x, x > 0
        alpha * (exp(x) -1), x<=0
        """
        alpha = 1
        elu = torch.nn.ELU(alpha=alpha, inplace=False)
        x = torch.arange(-10, 10, 1e-2)
        y = elu(x)
        plt.plot(x.detach(), y.detach(),
                 label="alpha={}".format(alpha), c='red')
        plt.grid(visible=True)
        plt.title("ELU()")
        plt.xlabel("x")
        plt.ylabel("y")
        plt.legend()
        plt.tight_layout()
        plt.show()

    def TestHardshrink(self):
        """
        Applies the Hard Shrinkage (Hardshrink) function element-wise.

        x, if x > lambda
        x, if x < - lambda
        0, otherwise
        """
        l = 5.0
        hard_shrink = torch.nn.Hardshrink(lambd=l)
        x = torch.arange(-10, 10, 1e-2)
        y = hard_shrink(x)
        plt.plot(x.detach(), y.detach(),
                 label="lambda={}".format(l), c='red')
        plt.grid(visible=True)
        plt.title("Hardshrink()")
        plt.xlabel("x")
        plt.ylabel("y")
        plt.legend()
        plt.tight_layout()
        plt.show()

    def TestHardsigmoid(self):
        """
        Applies the Hardsigmoid function element-wise.

        Hardsigmoid is defined as:

        0, x <= -3
        1, x >= +3
        x/6 + x/2 otherwise.
        """
        hard_sigmoid = torch.nn.Hardsigmoid()
        x = torch.arange(-10, 10, 1e-2)
        y = hard_sigmoid(x)
        plt.plot(x.detach(), y.detach(), c='red')
        plt.grid(visible=True)
        plt.title("Hardsigmoid()")
        plt.xlabel("x")
        plt.ylabel("y")
        # plt.legend()
        plt.tight_layout()
        plt.show()

    def TestHardtanh(self):
        """
        Applies the HardTanh function element-wise.

        max_val, if x > max_val;
        min_val, if x < min_val;
        x, otherwise.
        """
        hard_tanh = torch.nn.Hardtanh(min_val=-5, max_val=5)
        x = torch.arange(-10, 10, 1e-2)
        y = hard_tanh(x)
        plt.plot(x.detach(), y.detach(),
                 label="max_val={}, min_val={}".format(-5, 5), c='red')
        plt.grid(visible=True)
        plt.title("Hardsigmoid()")
        plt.xlabel("x")
        plt.ylabel("y")
        plt.legend()
        plt.tight_layout()
        plt.show()

    def TestHardswish(self):
        """
        Applies the hardswish function, element-wise, as described in the paper:

        0, <= -3;
        x, >= +3;
        x(x+3)/6, otherwise.
        """
        hardswish = torch.nn.Hardswish()
        x = torch.arange(-10, 10, 1e-2)
        y = hardswish(x)
        plt.plot(x.detach(), y.detach(), c='red')
        plt.grid(visible=True)
        plt.title("Hardsigmoid()")
        plt.xlabel("x")
        plt.ylabel("y")
        # plt.legend()
        plt.tight_layout()
        plt.show()

    def TestLeakyRELU(self):
        """
        Applies the element-wise function:

        LeakyReLU(x) = max(0,x) + negative_slope * min(0,x)

        x, x>=0
        negative_slope * x, otherwise.
        """
        negative_slope = 5e-2
        leaky_relu = torch.nn.LeakyReLU(negative_slope=negative_slope)
        x = torch.arange(-10, 10, 1e-2)
        y = leaky_relu(x)
        plt.plot(x.detach(), y.detach(),
                 label="negative_slope={}".format(negative_slope), c='red')
        plt.grid(visible=True)
        plt.title("LeakyRELU()")
        plt.xlabel("x")
        plt.ylabel("y")
        plt.legend()
        plt.tight_layout()
        plt.show()

    def TestLogSigmoid(self):
        """
        Applies the element-wise function:

        logsigmoid(x) = log(1/(1+exp(-x)))
        """
        log_sigmoid = torch.nn.LogSigmoid()
        x = torch.arange(-10, 10, 1e-2)
        y = log_sigmoid(x)
        plt.plot(x.detach(), y.detach(), c='red')
        plt.grid(visible=True)
        plt.title("LogSigmoid()")
        plt.xlabel("x")
        plt.ylabel("y")
        # plt.legend()
        plt.tight_layout()
        plt.show()

    def TestPRELU(self):
        """
        Applies the element-wise function:
        """
        init = 1e-1
        prelu = torch.nn.PReLU(init=init)
        x = torch.arange(-10, 10, 1e-2)
        y = prelu(x)
        plt.plot(x.detach(), y.detach(), label="init={}".format(init), c='red')
        plt.grid(visible=True)
        plt.title("PRELU()")
        plt.xlabel("x")
        plt.ylabel("y")
        plt.legend()
        plt.tight_layout()
        plt.show()

    def TestRELU(self):
        """
        Applies the rectified linear unit function element-wise:

        ReLU(x) = max(0, x)
        """
        relu = torch.nn.ReLU()
        x = torch.arange(-10, 10, 1e-2)
        y = relu(x)
        plt.plot(x.detach(), y.detach(), c='red')
        plt.grid(visible=True)
        plt.title("RELU()")
        plt.xlabel("x")
        plt.ylabel("y")
        # plt.legend()
        plt.tight_layout()
        plt.show()

    def TestRELU6(self):
        """
        Applies the element-wise function:
        ReLU6(x) = min(max(0, x), 6)
        """
        relu6 = torch.nn.ReLU6()
        x = torch.arange(-10, 10, 1e-2)
        y = relu6(x)
        plt.plot(x.detach(), y.detach(), c='red')
        plt.grid(visible=True)
        plt.title("RELU6()")
        plt.xlabel("x")
        plt.ylabel("y")
        # plt.legend()
        plt.tight_layout()
        plt.show()

    def TestRRELU(self):
        """
        Applies the randomized leaky rectified liner unit function,
        element-wise, as described in the paper

        x, if x >=0;
        ax, otherwise.
        """
        rrelu = torch.nn.RReLU()
        x = torch.arange(-10, 10, 1e-2)
        y = rrelu(x)
        plt.plot(x.detach(), y.detach(), c='red')
        plt.grid(visible=True)
        plt.title("RRELU()")
        plt.xlabel("x")
        plt.ylabel("y")
        # plt.legend()
        plt.tight_layout()
        plt.show()

    def TestSELU(self):
        """
        Applied element-wise, as
        SELU(x) = scale * (max(0,x)+min(0,alpha * (exp(x) -1)))
        """
        selu = torch.nn.SELU()
        x = torch.arange(-10, 10, 1e-2)
        y = selu(x)
        plt.plot(x.detach(), y.detach(), c='red')
        plt.grid(visible=True)
        plt.title("RRELU()")
        plt.xlabel("x")
        plt.ylabel("y")
        # plt.legend()
        plt.tight_layout()
        plt.show()

    def TestCELU(self):
        """
        Applies the element-wise function:

        CELU(x) = max(0,x) + min(0, alpha * (exp(x/a) -1 ))
        """
        celu = torch.nn.CELU()
        x = torch.arange(-10, 10, 1e-2)
        y = celu(x)
        plt.plot(x.detach(), y.detach(), c='red')
        plt.grid(visible=True)
        plt.title("CELU()")
        plt.xlabel("x")
        plt.ylabel("y")
        # plt.legend()
        plt.tight_layout()
        plt.show()

    def TestGELU(self):
        """
        Applies the Gaussian Error Linear Units function:
        GELU(x) = x * phi(x)
        phi(x) is the cumulative distribution function for gaussian distribution.
        """
        gelu = torch.nn.GELU()
        x = torch.arange(-10, 10, 1e-2)
        y = gelu(x)
        plt.plot(x.detach(), y.detach(), c='red')
        plt.grid(visible=True)
        plt.title("GELU()")
        plt.xlabel("x")
        plt.ylabel("y")
        # plt.legend()
        plt.tight_layout()
        plt.show()

    def TestMish(self):
        """
        Applies the Mish function, element-wise.
        Mish: A Self Regularized Non-Monotonic Neural Activation Function.
        """
        mish = torch.nn.Mish()
        x = torch.arange(-10, 10, 1e-2)
        y = mish(x)
        plt.plot(x.detach(), y.detach(), c='red')
        plt.grid(visible=True)
        plt.title("Mish()")
        plt.xlabel("x")
        plt.ylabel("y")
        # plt.legend()
        plt.tight_layout()
        plt.show()

    def TestSoftplus(self):
        """
        Applies the Softplus function:
        Softplus(x) = 1/b * log(1 + exp(b * x))
        SoftPlus is a smooth approximation to the ReLU function 
        and can be used to constrain the output of a machine to always be positive.
        """
        beta = 1.0
        threshold = 5
        softplus = torch.nn.Softplus()
        x = torch.arange(-10, 10, 1e-2)
        y = softplus(x)
        plt.plot(x.detach(), y.detach(), label="beta={}, threshold={}".format(
            beta, threshold), c='red')
        plt.grid(visible=True)
        plt.title("Softplus()")
        plt.xlabel("x")
        plt.ylabel("y")
        plt.legend()
        plt.tight_layout()
        plt.show()

    def TestSoftshrink(self):
        """
        Applies the soft shrinkage function elementwise:
        SoftShrinkage(x):
        x - lambda, if x > lambda
        x + lambda, if x < -lambda
        0, otherwise
        """
        l = 5
        soft_shrink = torch.nn.Softshrink(lambd=l)
        x = torch.arange(-10, 10, 1e-2)
        y = soft_shrink(x)
        plt.plot(x.detach(), y.detach(), label="lambda={}".format(l), c='red')
        plt.grid(visible=True)
        plt.title("Softshrink()")
        plt.xlabel("x")
        plt.ylabel("y")
        plt.legend()
        plt.tight_layout()
        plt.show()

    def TestSoftsign(self):
        """
        Applies the element-wise function:
        SoftSign(x): x / (1+|x|)
        """
        soft_sign = torch.nn.Softsign()
        x = torch.arange(-10, 10, 1e-2)
        y = soft_sign(x)
        plt.plot(x.detach(), y.detach(), c='red')
        plt.grid(visible=True)
        plt.title("Softsign()")
        plt.xlabel("x")
        plt.ylabel("y")
        # plt.legend()
        plt.tight_layout()
        plt.show()

    def TestTanh(self):
        """
        Applies the Hyperbolic Tangent (Tanh) function element-wise.

        Tanh(x) = (exp(x)-exp(-x))/(exp(x)+exp(-x))

        """
        tanh = torch.nn.Tanh()
        x = torch.arange(-10, 10, 1e-2)
        y = tanh(x)
        plt.plot(x.detach(), y.detach(), c='red')
        plt.grid(visible=True)
        plt.title("Tanh()")
        plt.xlabel("x")
        plt.ylabel("y")
        # plt.legend()
        plt.tight_layout()
        plt.show()

    def TestTanhshrink(self):
        """
        Applies the element-wise function:
        Tanhshrink(x)=x-tanh(x)
        """
        tanh_shrink = torch.nn.Tanhshrink()
        x = torch.arange(-10, 10, 1e-2)
        y = tanh_shrink(x)
        plt.plot(x.detach(), y.detach(), c='red')
        plt.grid(visible=True)
        plt.title("Tanhshrink()")
        plt.xlabel("x")
        plt.ylabel("y")
        # plt.legend()
        plt.tight_layout()
        plt.show()

    def TestThreshold(self):
        """
        Thresholds each element of the input Tensor.

        x, if x > threshold,
        value, otherwise.
        """
        value = 2
        threshold_value = 4
        threshold = torch.nn.Threshold(value=value, threshold=threshold_value)
        x = torch.arange(-10, 10, 1e-2)
        y = threshold(x)
        plt.plot(x.detach(), y.detach(), label="value={}, threshold={}".format(
            value, threshold_value), c='red')
        plt.grid(visible=True)
        plt.title("Threshold()")
        plt.xlabel("x")
        plt.ylabel("y")
        plt.legend()
        plt.tight_layout()
        plt.show()

    def TestGLU(self):
        """
        Applies the gated linear unit function
        """
        glu = torch.nn.GLU()
        x = torch.arange(-10, 10, 1e-2)
        y = glu(x)
        plt.plot(x.detach(), c='red')
        plt.plot(y.detach(), c='blue')
        plt.grid(visible=True)
        plt.title("GLU()")
        plt.xlabel("x")
        plt.ylabel("y")
        # plt.legend()
        plt.tight_layout()
        plt.show()

    def TestSoftmin(self):
        """
        Applies the Softmin function to an n-dimensional input Tensor rescaling them 
        so that the elements of the n-dimensional output Tensor lie in the range [0, 1]
        and sum to 1.

        softmin(xi) = exp(-xi)/sigma(-xj)
        """
        softmin = torch.nn.Softmin()
        x = torch.tensor([8, 6, 2, 1, 5, 7, 9, 3, 0, 4], dtype=torch.float64)
        y = softmin(x)
        plt.stem(y.detach())
        plt.grid(visible=True)
        plt.title("Softmin()")
        plt.xlabel("x")
        plt.ylabel("y")
        # plt.legend()
        plt.tight_layout()
        plt.show()

    def TestSoftmax(self):
        """
        Applies the Softmax function to an n-dimensional input Tensor rescaling them 
        so that the elements of the n-dimensional output Tensor lie in the range [0,1] and sum to 1.
        """
        softmax = torch.nn.Softmax()
        x = torch.tensor([8, 6, 2, 1, 5, 7, 9, 3, 0, 4], dtype=torch.float64)
        y = softmax(x)
        plt.stem(y.detach())
        plt.grid(visible=True)
        plt.title("Softmax()")
        plt.xlabel("x")
        plt.ylabel("y")
        # plt.legend()
        plt.tight_layout()
        plt.show()

    def TestSoftmax2D(self):
        """
        Applies SoftMax over features to each spatial location.

        When given an image of Channels x Height x Width, it will apply Softmax to each location 
        """
        soft_max2d = torch.nn.Softmax2d()
        x = 10 * torch.randn(size=(3, 16, 16))
        y = soft_max2d(x)
        fig, axs = plt.subplots(nrows=2, ncols=3)
        for i in range(3):
            axs[0][i].imshow(x[i])
            axs[1][i].imshow(y[i])
        plt.tight_layout()
        plt.show()

    def TestLogSoftmax(self):
        """
        Applies the log(Softmax(x))log(Softmax(x)) function to an n-dimensional input Tensor. 


        a Tensor of the same dimension and shape as the input with values in the range [-inf, 0)
        """
        log_softmax = torch.nn.LogSigmoid()
        x = torch.tensor([8, 6, 2, 1, 5, 7, 9, 3, 0, 4], dtype=torch.float64)
        y = log_softmax(x)
        plt.stem(y.detach())
        plt.grid(visible=True)
        plt.title("LogSoftmax()")
        plt.xlabel("x")
        plt.ylabel("y")
        # plt.legend()
        plt.tight_layout()
        plt.show()

    def TestAdaptiveLogSoftmaxWithLoss(self):
        pass

    def tearDown(self) -> None:
        return super().tearDown()


def CreateActivationTestSuit():
    suit = unittest.TestSuite()
    # suit.addTest(ActivationTestCase("TestELU"))
    # suit.addTest(ActivationTestCase("TestHardshrink"))
    # suit.addTest(ActivationTestCase("TestHardsigmoid"))
    # suit.addTest(ActivationTestCase("TestHardtanh"))
    # suit.addTest(ActivationTestCase("TestHardswish"))
    # suit.addTest(ActivationTestCase("TestLeakyRELU"))
    # suit.addTest(ActivationTestCase("TestLogSigmoid"))
    # suit.addTest(ActivationTestCase("TestPRELU"))
    # suit.addTest(ActivationTestCase("TestRELU"))
    # suit.addTest(ActivationTestCase("TestRELU6"))
    # suit.addTest(ActivationTestCase("TestRRELU"))
    # suit.addTest(ActivationTestCase("TestSELU"))
    # suit.addTest(ActivationTestCase("TestCELU"))
    # suit.addTest(ActivationTestCase("TestGELU"))
    # suit.addTest(ActivationTestCase("TestMish"))
    # suit.addTest(ActivationTestCase("TestSoftplus"))
    # suit.addTest(ActivationTestCase("TestSoftshrink"))
    # suit.addTest(ActivationTestCase("TestSoftsign"))
    # suit.addTest(ActivationTestCase("TestTanh"))
    # suit.addTest(ActivationTestCase("TestTanhshrink"))
    # suit.addTest(ActivationTestCase("TestThreshold"))
    # suit.addTest(ActivationTestCase("TestGLU"))
    # EYE
    # suit.addTest(ActivationTestCase("TestSoftmin"))
    # suit.addTest(ActivationTestCase("TestSoftmax"))
    # suit.addTest(ActivationTestCase("TestSoftmax2D"))
    # suit.addTest(ActivationTestCase("TestLogSoftmax"))
    suit.addTest(ActivationTestCase("TestAdaptiveLogSoftmaxWithLoss"))
    return suit


if __name__ == "__main__":
    unittest.TextTestRunner().run(CreateActivationTestSuit())
