from asyncio import trsock
import torch
import unittest


class LinearLayersTestCase(unittest.TestCase):
    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)

    def setUp(self) -> None:
        return super().setUp()

    def TestIdentity(self):
        """
        A placeholder identity operator that is argument-insensitive.

        output is the same as input.
        """
        identity = torch.nn.Identity()
        t = torch.arange(0, 10, 1.)
        o = identity(t)
        self.assertTrue(torch.equal(t, o))

    def TestLinear(self):
        """
        Applies a linear transformation to the incoming data:

        y = x * A.T + b

        parameters:
        in_features - size of each input sample
        out_features - size of each output sample
        bias - If set to False, the layer will not learn an additive bias. Default: True

        """
        # 0.
        linear = torch.nn.Linear(in_features=4,  out_features=4)
        x = torch.linspace(0, 4, 4)
        y = linear(x)

        y_ = x @ linear.weight.mT + linear.bias  # 1 x 4 @ 4 x 4 -> 1 x 4
        self.assertEqual(y.shape, (4,))
        self.assertTrue(torch.equal(y, y_))
        # 1.
        linear = torch.nn.Linear(in_features=4, out_features=8)
        x = torch.linspace(0, 4, 4)
        y = linear(x)
        y_ = x @ linear.weight.mT + linear.bias
        self.assertEqual(y.shape, (8,))
        self.assertEqual(y_.shape, (8,))
        self.assertTrue(torch.equal(y, y_))

    def TestBilinear(self):
        """
        Applies a bilinear transformation to the incoming data:
        y = x1.T @ A @ x2 + b

        parameters:
        """
        bilinear = torch.nn.Bilinear(
            in1_features=4, in2_features=8, out_features=6)
        x1 = torch.linspace(0, 4, 4)
        x2 = torch.linspace(0, 4, 8)
        y = bilinear(x1, x2)
        y_ = (x1.T) @ bilinear.weight @ x2 + bilinear.bias
        # self.assertTrue(torch.equal(y, y_))

    def TestLazyLinear(self):
        """
        A torch.nn.Linear module where in_features is inferred.

        In this module, the weight and bias are of torch.nn.UninitializedParameter class.
        They will be initialized after the first call to forward is done and the module will become a regular torch.nn.Linear module.
        The in_features argument of the Linear is inferred from the input.shape[-1].

        parameters:

        out_features - size of each output sample
        bias - If set to False, the layer will not learn an additive bias. Default: True
        """
        lazy_linear = torch.nn.LazyLinear(out_features=6)
        x = torch.linspace(0, 4, 8)
        y = lazy_linear(x)
        self.assertEqual(lazy_linear.weight.shape, (6, 8))

    def tearDown(self) -> None:
        return super().tearDown()


def CreateLinearLayersTestSuit():
    suit = unittest.TestSuite()
    suit.addTest(LinearLayersTestCase("TestIdentity"))
    suit.addTest(LinearLayersTestCase("TestLinear"))
    suit.addTest(LinearLayersTestCase("TestBilinear"))
    suit.addTest(LinearLayersTestCase("TestLazyLinear"))
    return suit


if __name__ == "__main__":
    unittest.TextTestRunner().run(CreateLinearLayersTestSuit())
