from copy import copy
from math import ceil
from multiprocessing.resource_sharer import stop
from turtle import shape
import unittest
import torch
import matplotlib.pyplot as plt
import numpy as np


class TensorTestCase(unittest.TestCase):
    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)

    def setUp(self) -> None:
        return super().setUp()

    def TestTensor(self):
        """
        Constructs a tensor with no autograd history (also known as a “leaf tensor”, see Autograd mechanics) by COPYTING data.

        When working with tensors prefer using torch.Tensor.clone(), 
        torch.Tensor.detach(), and torch.Tensor.requires_grad_() for readability. 
        Letting t be a tensor, torch.tensor(t) is equivalent to t.clone().detach(), 
        and torch.tensor(t, requires_grad=True) is equivalent to t.clone().detach().requires_grad_(True).

        torch.as_tensor() preserves autograd history and avoids copies where possible. 
        torch.from_numpy() creates a tensor that shares storage with a NumPy array.

        parameters:

        data (array_like) - Initial data for the tensor. Can be a list, tuple, NumPy ndarray, scalar, and other types.
        dtype (torch.dtype, optional) - the desired data type of returned tensor. Default: if None, infers data type from data.
        device (torch.device, optional) - the device of the constructed tensor. If None and data is a tensor then the device of data is used. 
            If None and data is not a tensor then the result tensor is constructed on the CPU.
        requires_grad (bool, optional) - If autograd should record operations on the returned tensor. Default: False.
        pin_memory (bool, optional) - If set, returned tensor would be allocated in the pinned memory. Works only for CPU tensors. Default: False.
        """
        # 0.
        # pin_memory is requires cuda.
        t = torch.tensor([1, 2, 3], dtype=torch.float64,
                         device="cpu", requires_grad=True)
        self.assertTrue(t.shape, (3,))
        arr = [[1, 2, 3], [4, 5, 6]]
        t = torch.tensor(data=arr)  # copy.
        t[0, 0] = -1
        self.assertEqual(arr[0][0], 1)
        self.assertEqual(t[0][0], -1)

        # torch.tensor always COPY.

    def TestAsarray(self):
        """
        Converts obj to a tensor.

        obj can be one of:

        a tensor
        a NumPy array
        a DLPack capsule
        an object that implements Python’s buffer protocol
        a scalar
        a sequence of scalars

        When obj is a tensor, NumPy array, or DLPack capsule the returned tensor will, 
        by default, not require a gradient, have the same datatype as obj, be on the same device, 
        and SHARE memory with it. 

        parameters:
        obj (object) - a tensor, NumPy array, DLPack Capsule, object that implements Python's buffer protocol, scalar, or sequence of scalars.

        dtype (torch.dtype, optional) - the datatype of the returned tensor. 
            Default: None, which causes the datatype of the returned tensor to be inferred from obj.
        copy (bool, optional) - controls whether the returned tensor shares memory with obj. 
            Default: None, which causes the returned tensor to share memory with obj whenever possible. 
            If True then the returned tensor does not share its memory. 
            If False then the returned tensor shares its memory with obj and an error is thrown if it cannot.
        device (torch.device, optional) - the device of the returned tensor. 
            Default: None, which causes the device of obj to be used.
        requires_grad (bool, optional) - whether the returned tensor requires grad. 
            Default: False, which causes the returned tensor not to require a gradient. 
            If True, then the returned tensor will require a gradient, 
            and if obj is also a tensor with an autograd history then the returned tensor will have the same history.
        """
        # 0. tensor
        t = torch.tensor([1, 2, 3, 4, 5, 6, 7, 8, 9, 0])
        u = torch.asarray(t)
        t[5] = 42
        self.assertEqual(u[5], 42)  # SHARE memory.
        # 1. np.array
        n = np.arange(0, 10)
        t = torch.asarray(n)
        n[5] = 42
        self.assertEqual(t[5], 42)

        # force copy.
        n = np.arange(0, 10)
        t = torch.asarray(n, copy=True)
        n[5] = 42
        self.assertNotEqual(t[5], 42)

    def TestAsTensor(self):
        """
        Converts data into a tensor, sharing data and preserving autograd history if possible.

        If data is already a tensor with the requeseted dtype and device then data itself is returned,
        but if data is a tensor with a different dtype or device then it's copied as if using data.to(dtype=dtype, device=device).

        If data is a NumPy array (an ndarray) with the same dtype and device then a tensor is constructed using torch.from_numpy().

        comparing: torch.tensor() never shares its data and creates a new “leaf tensor”

        parameters:

        data (array_like) - Initial data for the tensor. Can be a list, tuple, NumPy ndarray, scalar, and other types.
        dtype (torch.dtype, optional) - the desired data type of returned tensor. 
            Default: if None, infers data type from data.
        device (torch.device, optional) - the device of the constructed tensor. 
            If None and data is a tensor then the device of data is used. 
            If None and data is not a tensor then the result tensor is constructed on the CPU.
        """
        # 0. list
        data = [1, 2, 3, 4, 5]
        t = torch.as_tensor(data=data)
        data[3] = 42
        self.assertNotEqual(data[3], t[3])  # not shared with list
        # 1. tensor
        t = torch.tensor([1, 2, 3, 4, 5])
        u = torch.as_tensor(t)  # shared with tensor.
        # 2. numpy array
        n = np.array([1, 2, 3, 4, 5])
        t = torch.as_tensor(n)
        n[3] = 42
        self.assertEqual(n[3], t[3])  # shared with numpy array.

    def TestAsStrided(self):
        """
        Create a VIEW of an existing torch.Tensor input with specified size, stride and storage_offset.

        Prefer using other view functions, like torch.Tensor.expand(), to setting a view's strides manually with as_strided, 
        as this function's behavior depends on the implementation of a tensor's storage.
        The constructed view of the storage must only refer to elements within the storage or a runtime error will be thrown, 
        and if the view is “overlapped” (with multiple indices referring to the same element in memory) its behavior is undefined.

        """
        t = torch.tensor([1, 2, 3, 4, 5, 6, 7, 8, 9])
        t = torch.as_strided(t, size=(2,), stride=(2,))

    def TestFromNumpy(self):
        """
        Creates a Tensor from a numpy.ndarray.

        The returned tensor and ndarray share the same memory. 
        Modifications to the tensor will be reflected in the ndarray and vice versa. 
        The returned tensor is NOT resizable.

        It currently accepts ndarray with dtypes of 
        numpy.float64, numpy.float32, numpy.float16, 
        numpy.complex64, numpy.complex128, numpy.int64,
        numpy.int32, numpy.int16, numpy.int8, numpy.uint8, and numpy.bool.
        """
        n = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
        t = torch.from_numpy(n)
        n[4] = 42
        self.assertEqual(t[4], 42)  # shared

    def TestZeros(self):
        """
        Returns a tensor filled with the scalar value 0, with the shape defined by the variable argument size.

        parameters:
        size (int...) - a sequence of integers defining the shape of the output tensor.
        Can be a variable number of arguments or a collection like a list or tuple.

        out (Tensor, optional) - the output tensor.
        dtype (torch.dtype, optional) - the desired data type of returned tensor. 
            Default: if None, uses a global default (see torch.set_default_tensor_type()).
        layout (torch.layout, optional) - the desired layout of returned Tensor. 
            Default: torch.strided.
        device (torch.device, optional) - the desired device of returned tensor.
            Default: if None, uses the current device for the default tensor type (see torch.set_default_tensor_type()). 
            device will be the CPU for CPU tensor types and the current CUDA device for CUDA tensor types.
        requires_grad (bool, optional) - If autograd should record operations on the returned tensor. 
            Default: False.
        """
        t = torch.zeros(size=(5, 5))
        for row in t:
            self.assertTrue(all(row == 0))

    def TestZerosLike(self):
        """
        Returns a tensor filled with the scalar value 0, with the same size as input.
        torch.zeros_like(input) is equivalent to 
        torch.zeros(input.size(), dtype=input.dtype, layout=input.layout, device=input.device).
        """

        t = torch.tensor([[1, 2, 3], [4, 5, 6]])
        u = torch.zeros_like(t)
        self.assertEqual(u.shape, t.shape)
        for row in u:
            self.assertTrue(any(row == 0))

    def TestOnes(self):
        """
        Returns a tensor filled with the scalar value 1, with the shape defined by the variable argument size.

        paramenters:
        size (int...) - a sequence of integers defining the shape of the output tensor.
        Can be a variable number of arguments or a collection like a list or tuple.

        out (Tensor, optional) - the output tensor.
        dtype (torch.dtype, optional) - the desired data type of returned tensor.
            Default: if None, uses a global default (see torch.set_default_tensor_type()).
        layout (torch.layout, optional) - the desired layout of returned Tensor. 
            Default: torch.strided.
        device (torch.device, optional) - the desired device of returned tensor. 
            Default: if None, uses the current device for the default tensor type (see torch.set_default_tensor_type()). 
            device will be the CPU for CPU tensor types and the current CUDA device for CUDA tensor types.
        requires_grad (bool, optional) - If autograd should record operations on the returned tensor.
            Default: False.
        """
        t = torch.ones(size=(4, 4))
        self.assertEqual(t.shape, (4, 4))
        for row in t:
            self.assertTrue(any(row == 1))

    def TestOnesLike(self):
        """
        Returns a tensor filled with the scalar value 1, with the same size as input. 
        torch.ones_like(input) is equivalent to 
        torch.ones(input.size(), dtype=input.dtype, layout=input.layout, device=input.device).
        """
        t = torch.tensor([[1, 2, 3], [4, 5, 6]])
        u = torch.ones_like(t)
        self.assertEqual(u.shape, u.shape)
        for row in u:
            self.assertTrue(any(row == 1))

    def TestEmpty(self):
        """
        Returns a tensor filled with uninitialized data. The shape of the tensor is defined by the variable argument size.

        """
        t = torch.empty(size=(4, 4))
        self.assertEqual(t.shape, (4, 4))

    def TestEmptyLike(self):
        """
        Returns an uninitialized tensor with the same size as input. 
        torch.empty_like(input) is equivalent to torch.empty(input.size(), dtype=input.dtype, layout=input.layout, device=input.device).
        """
        t = torch.tensor([[1, 2, 3], [4, 5, 6]])
        u = torch.empty_like(t)
        self.assertEqual(u.shape, u.shape)

    def TestFull(self):
        """
        Creates a tensor of size size filled with fill_value. The tensor’s dtype is inferred from fill_value.

        parameters:
        size (int...) - a list, tuple, or torch.Size of integers defining the shape of the output tensor.
        fill_value (Scalar) - the value to fill the output tensor with.
        ...
        """
        t = torch.full(size=(4, 4), fill_value=8)
        self.assertEqual(t.shape, (4, 4))
        for row in t:
            self.assertTrue(any(row == 8))

    def TestFullLike(self):
        """
        Returns a tensor with the same size as input filled with fill_value.
        torch.full_like(input, fill_value) is equivalent to 
        torch.full(input.size(), fill_value, dtype=input.dtype, layout=input.layout, device=input.device).
        """
        t = torch.tensor([[1, 2, 3], [4, 5, 6]])
        u = torch.full_like(t, fill_value=8)
        self.assertEqual(u.shape, u.shape)
        for row in u:
            self.assertTrue(any(row == 8))

    def TestEye(self):
        """
        Returns a 2-D tensor with ones on the diagonal and zeros elsewhere.

        parameters:
        n (int) - the number of rows
        m (int, optional) - the number of columns with default being n
        ...
        """
        t = torch.eye(4, 4)
        for i in range(4):
            self.assertEqual(t[i, i], 1)

    def TestArange(self):
        """
        Returns a 1-D tensor of size ceil((end - sart)/step) with with values from the interval [start, end) 
        taken with common difference step beginning from start.

        parameters:

        start (Number) - the starting value for the set of points. Default: 0.
        end (Number) - the ending value for the set of points
        step (Number) - the gap between each pair of adjacent points. Default: 1.
        """
        t = torch.arange(start=0, end=2, step=0.1)
        self.assertEqual(len(t), ceil((2 - 0) / 0.1))

    def TestLinspace(self):
        """
        Creates a one-dimensional tensor of size steps whose values are evenly spaced from start to end, inclusive. 

        parameters:

        start (float) - the starting value for the set of points
        end (float) - the ending value for the set of points
        steps (int) - size of the constructed tensor
        """
        t = torch.linspace(start=0, end=10, steps=100)
        self.assertEqual(len(t), 100)

    def TestLogspace(self):
        """
        Creates a one-dimensional tensor of size steps whose values are evenly spaced from base ** start to base ** end, 
        inclusive, on a logarithmic scale with base base.

        paramenters:

        start (float) - the starting value for the set of points
        end (float) - the ending value for the set of points
        steps (int) - size of the constructed tensor
        base (float, optional) - base of the logarithm function. Default: 10.0.

        ...
        """
        t = torch.logspace(start=0, end=10, steps=100)
        self.assertEqual(len(t), 100)

    def TestSqueeze(self):
        """
        Returns a tensor with all the dimensions of input of size 1 removed.
        When dim is given, a squeeze operation is done only in the given dimension.

        examples:

        shape: (n, 1, m) -> (n, m) OK
        shape: (n, 2, m) -> (n, 2, m) Won't work. 

        The returned tensor SHARES the storage with the input tensor, 
        so changing the contents of one will change the contents of the other.
        """
        t = torch.rand(size=(2, 1, 2, 1, 2))
        u = t.squeeze()
        self.assertEqual(u.shape, (2, 2, 2))
        v = t.squeeze(1)
        self.assertEqual(v.shape, (2, 2, 1, 2))

    def TestUnsqueeze(self):
        """
        Returns a new tensor with a dimension of size one inserted at the specified position.

        The returned tensor SHARES the same underlying data with this tensor.

        A dim value within the range [-input.dim() - 1, input.dim() + 1) can be used. 
        Negative dim will correspond to unsqueeze() applied at dim = dim + input.dim() + 1.

        parameters:

        input (Tensor) - the tensor to unbind
        dim (int) - dimension to remove
        """
        t = torch.tensor([[1, 2, 3], [4, 5, 6]])
        u = torch.unsqueeze(t, dim=0)
        self.assertEqual(u.shape, (1, 2, 3))
        t[0, 0] = 42
        self.assertEqual(u[0, 0, 0], 42)

    def TestUnbind(self):
        """
        Removes a tensor dimension.

        Returns a tuple of all slices along a given dimension, already without it.
        """
        t = torch.tensor([[1, 2, 3], [4, 5, 6]])
        u = torch.unbind(t, dim=1)
        self.assertEqual(len(u), 3)

    def TestTranspose(self):
        """
        Returns a tensor that is a transposed version of input. The given dimensions dim0 and dim1 are swapped.

        If input is a strided tensor then the resulting out tensor SHARES its underlying storage with the input tensor, 
        so changing the content of one would change the content of the other.

        If input is a sparse tensor then the resulting out tensor does NOT SHARE the underlying storage with the input tensor.
        """
        t = torch.tensor([[1, 2, 3, 4], [5, 6, 7, 8]])
        u = torch.transpose(t, dim0=1, dim1=0)
        # u = t.transpose(dim0=0, dim1=1)
        self.assertEqual(t.shape, (2, 4))
        self.assertEqual(u.shape, (4, 2))
        t[0, 0] = 10
        self.assertEqual(u[0, 0], 10)
        # equivalent: torch.swapaxes(), torch.swapdims()

    def TestT(self):
        """
        Expects input to be <= 2-D tensor and transposes dimensions 0 and 1.

        0-D and 1-D tensors are returned as is. When input is a 2-D tensor this is equivalent to transpose(input, 0, 1).
        """
        t = torch.tensor([[1, 2, 3, 4], [5, 6, 7, 8]])
        u = t.t()
        self.assertEqual(t.shape, (2, 4))
        self.assertEqual(u.shape, (4, 2))
        t[0, 0] = 10
        self.assertEqual(u[0, 0], 10)

    def TestStack(self):
        """
        Concatenates a sequence of tensors along a new dimension.

        All tensors need to be of the same size.
        """
        u = torch.tensor([1, 2, 3, 4, 5])
        v = torch.tensor([5, 4, 3, 2, 1])
        w = torch.stack((u, v), dim=0)
        self.assertEqual(w.shape, (2, 5))
        w = torch.stack((u, v), dim=1)
        self.assertEqual(w.shape, (5, 2))

    def TestVstack(self):
        """
        Stack tensors in sequence vertically (row wise).

        This is equivalent to concatenation along the first axis after all 1-D tensors have been reshaped by torch.atleast_2d()
        """
        u = torch.tensor([[1, 2, 3],
                          [4, 5, 6]])
        v = torch.tensor([[-1, -2, -3],
                          [-4, -5, -6]])
        w = torch.vstack((u, v))
        self.assertEqual(u.shape, (2, 3))
        self.assertEqual(v.shape, (2, 3))
        self.assertEqual(w.shape, (4, 3))
        self.assertEqual(w[0, 0], u[0, 0])
        self.assertEqual(w[2, 0], v[0, 0])

        # equivalent: torch.row_stack()

    def TestHstack(self):
        """
        Stack tensors in sequence horizontally (column wise).

        This is equivalent to concatenation along the first axis for 1-D tensors, and along the second axis for all other tensors.        
        """
        u = torch.tensor([[1, 2, 3],
                          [4, 5, 6]])
        v = torch.tensor([[-1, -2, -3],
                          [-4, -5, -6]])
        w = torch.hstack((u, v))
        self.assertEqual(u.shape, (2, 3))
        self.assertEqual(v.shape, (2, 3))
        self.assertEqual(w.shape, (2, 6))
        self.assertEqual(w[0, 0], u[0, 0])
        self.assertEqual(w[0, 3], v[0, 0])

        # equivalent: torch.column_stack()

    def TestSplit(self):
        """
        Splits the tensor into chunks. Each chunk is a VIEW of the original tensor.


        If split_size_or_sections is an integer type, 
        then tensor will be split into equally sized chunks (if possible).
        Last chunk will be smaller if the tensor size along the given dimension dim is not divisible by split_size.

        If split_size_or_sections is a list, 
        then tensor will be split into len(split_size_or_sections) chunks with sizes in dim according to split_size_or_sections.
        """
        # 0.
        t = torch.tensor([[1, 2, 3, 4, 5], [6, 7, 8, 9, 10]])
        u, v, w = torch.split(t, split_size_or_sections=2, dim=1)
        self.assertEqual(u.shape, (2, 2))
        self.assertEqual(v.shape, (2, 2))
        self.assertEqual(w.shape, (2, 1))
        # 1.
        t = torch.tensor([[1, 2, 3, 4, 5], [6, 7, 8, 9, 10]])
        u, v, w = torch.split(t, [1, 1, 3], dim=1)
        self.assertEqual(u.shape, (2, 1))
        self.assertEqual(v.shape, (2, 1))
        self.assertEqual(w.shape, (2, 3))

    def TestTensorSplit(self):
        """
        Splits a tensor into multiple sub-tensors, 
        all of which are VIEWS of input, 
        along dimension dim according to the indices or number of sections specified by indices_or_sections. 

        This function is based on NumPy’s numpy.array_split().

        If indices_or_sections is an integer n or a zero dimensional long tensor with value n,
        input is split into n sections along dimension dim.
        If input is divisible by n along dimension dim,
        each section will be of equal size, input.size(dim) / n.

        If input is not divisible by n, 
        the sizes of the first int(input.size(dim) % n) sections will have size int(input.size(dim) / n) + 1, 
        and the rest will have size int(input.size(dim) / n).

        If indices_or_sections is a list or tuple of ints, 
        or a one-dimensional long tensor, 
        then input is split along dimension dim at each of the indices in the list, 
        tuple or tensor. For instance, 
        indices_or_sections=[2, 3] and dim=0 would result in the tensors input[:2], input[2:3], and input[3:].

        If indices_or_sections is a tensor, it must be a zero-dimensional or one-dimensional long tensor on the CPU.
        """
        # 0.
        t = torch.tensor([[1, 2, 3, 4, 5], [6, 7, 8, 9, 10]])
        u, v, w = torch.tensor_split(t, sections=3, dim=1)  # sections = 3
        self.assertEqual(u.shape, (2, 2))
        self.assertEqual(v.shape, (2, 2))
        self.assertEqual(w.shape, (2, 1))

        # 1.
        t = torch.tensor([[1, 2, 3, 4, 5], [6, 7, 8, 9, 10]])
        indices = [1, 4]
        u, v, w = torch.tensor_split(t, indices=indices, dim=1)
        self.assertEqual(u.shape, (2, 1))
        self.assertEqual(v.shape, (2, 3))
        self.assertEqual(w.shape, (2, 1))

    def TestDsplit(self):
        """
        Splits input, a tensor with three or more dimensions, 
        into multiple tensors depthwise according to indices_or_sections.
        Each split is a VIEW of input.

        This is equivalent to calling torch.tensor_split(input, indices_or_sections, dim=2) (the split dimension is 2),
        except that if indices_or_sections is an integer it must evenly divide the split dimension or a runtime error will be thrown.

        This function is based on NumPy's numpy.dsplit().

        tensor (Tensor) - tensor to split.
        split_size_or_sections (int) or (list(int)) - size of a single chunk or list of sizes for each chunk
        dim (int) - dimension along which to split the tensor.

        parameters:
        input (Tensor) - tensor to split.
        indices_or_sections (Tensor, int or list or tuple of python:ints) - See argument in torch.tensor_split().
        """
        t = torch.arange(16.0).reshape(2, 2, 4)  # 'd' means depth
        u, v, w, x = torch.dsplit(t, sections=4)
        self.assertEqual(u.shape, (2, 2, 1))
        self.assertEqual(v.shape, (2, 2, 1))
        self.assertEqual(w.shape, (2, 2, 1))
        self.assertEqual(x.shape, (2, 2, 1))

    def TestHsplit(self):
        """
        Splits input, a tensor with one or more dimensions, 
        into multiple tensors horizontally according to indices_or_sections. 
        Each split is a VIEW of input.

        If input is one dimensional this is equivalent to calling torch.tensor_split(input, indices_or_sections, dim=0) (the split dimension is zero), 
        and if input has two or more dimensions it's equivalent to calling torch.tensor_split(input, indices_or_sections, dim=1) (the split dimension is 1), 
        except that if indices_or_sections is an integer it must evenly divide the split dimension or a runtime error will be thrown.

        This function is based on NumPy's numpy.hsplit().
        """
        t = torch.arange(16.0).reshape(4, 4)
        u, v, w, x = torch.hsplit(t, sections=4)
        self.assertEqual(u.shape, (4, 1))
        self.assertEqual(v.shape, (4, 1))
        self.assertEqual(w.shape, (4, 1))
        self.assertEqual(x.shape, (4, 1))

    def TestTake(self):
        """
        Returns a new tensor with the elements of input at the given indices.
        The input tensor is treated as if it were viewed as a 1-D tensor. 
        The result takes the same shape as the indices.

        input (Tensor) - the input tensor.
        index (LongTensor) - the indices into tensor
        """
        t = torch.tensor([[1, 2, 3], [4, 5, 6]])
        # index must not out of range.
        u = torch.take(t, torch.tensor([[1, 2, 3]]))  # u = [2, 3, 4]

    def TestComplex(self):
        """
        Constructs a complex tensor with its real part equal to real and its imaginary part equal to imag.

        parameters:
        real (Tensor) - The real part of the complex tensor. Must be float or double.
        imag (Tensor) - The imaginary part of the complex tensor. Must be same dtype as real.
        """
        r = torch.arange(0, 5, 1.0)
        i = torch.arange(-5, 0, 1.0)
        c = torch.complex(real=r, imag=i)
        for cc, rr, ii in zip(c, r, i):
            self.assertEqual(cc, rr+ii*1j)

    def TestAdjont(self):
        """
        Returns a VIEW of the tensor conjugated and with the last two dimensions transposed.

        x.adjoint() is equivalent to x.transpose(-2, -1).conj() for complex tensors and to x.transpose(-2, -1) for real tensors.
        """
        r = torch.arange(0, 4, 1.0).reshape(2, 2)
        i = torch.arange(-4, 0, 1.0).reshape(2, 2)
        c = torch.complex(r, i)
        a = torch.adjoint(c)

    def TestArgwhere(self):
        """
        Returns a tensor containing the indices of all non-zero elements of input. 
        Each row in the result contains the indices of a non-zero element in input. 
        The result is sorted lexicographically, with the last index changing the fastest (C-style).
        """
        t = torch.tensor([[1, 0, 2, 0], [0, 3, 0, 4]])
        incs = torch.argwhere(t)  # NON-ZERO INDEX

    def TestCat(self):
        """
        Concatenates the given sequence of seq tensors in the given dimension. 
        All tensors must either have the same shape (except in the concatenating dimension) or be empty.

        torch.cat() can be seen as an inverse operation for torch.split() and torch.chunk().

        torch.cat() can be best understood via examples.

        parameters:
        tensors (sequence of Tensors) - any python sequence of tensors of the same type. 
        Non-empty tensors provided must have the same shape, except in the cat dimension.

        dim (int, optional) - the dimension over which the tensors are concatenated
        ...
        """
        seq = (torch.ones(size=(4, 4, 4)), torch.ones(
            size=(4, 4, 4)))  # MUST be tuple rather be tensor.
        t = torch.cat(seq)

    def TestChunk(self):
        """
        Attempts to split a tensor into the specified number of chunks. Each chunk is a VIEW of the input tensor.

        This function may return less then the specified number of chunks!

        parameters:

        input (Tensor) - the tensor to split
        chunks (int) - number of chunks to return
        dim (int) - dimension along which to split the tensor

        If the tensor size along the given dimesion dim is divisible by chunks, all returned chunks will be the same size.
        If the tensor size along the given dimension dim is not divisible by chunks, all returned chunks will be the same size, except the last one. 
        If such division is not possible, this function may return less than the specified number of chunks.
        """
        t = torch.ones(size=(4, 4))
        # chunk
        u, v = torch.chunk(input=t, chunks=2, dim=0)
        self.assertEqual(u.shape, (2, 4))
        self.assertEqual(v.shape, (2, 4))
        # tensor_split
        w, x = torch.tensor_split(input=t, sections=2, dim=0)
        self.assertEqual(w.shape, (2, 4))
        self.assertEqual(x.shape, (2, 4))

        # Not divisible
        t = torch.ones(size=(8, 4))
        # chunk
        u, v, w = torch.chunk(input=t, chunks=3, dim=0)
        self.assertEqual(u.shape, (3, 4))
        self.assertEqual(v.shape, (3, 4))
        self.assertEqual(w.shape, (2, 4))
        x, y, z = torch.tensor_split(input=t, sections=3, dim=0)
        # tensor_split()
        self.assertEqual(x.shape, (3, 4))
        self.assertEqual(y.shape, (3, 4))
        self.assertEqual(z.shape, (2, 4))
        # torch.split()?
        a, b, c = torch.split(t, 3, dim=0)  # input
        self.assertEqual(a.shape, (3, 4))
        self.assertEqual(b.shape, (3, 4))
        self.assertEqual(c.shape, (2, 4))
        t[0, 0] = 42
        self.assertEqual(u[0, 0], 42)
        self.assertEqual(x[0, 0], 42)
        self.assertEqual(a[0, 0], 42)

    def TestWhere(self):
        """
        Return a tensor of elements selected from either x or y, depending on condition.

        The operation is defined as:
        x, if conditon is ture; y, if condition is false.
        """
        t = torch.arange(0, 10, 1.0)
        u = torch.where(t > 5, 1, 0)
        self.assertEqual(sum(u), 4)

    def TestTile(self):
        """
        Constructs a tensor by repeating the elements of input. 
        The dims argument specifies the number of repetitions in each dimension.

        If dims specifies fewer dimensions than input has, 
        then ones are prepended to dims until all dimensions are specified. For example, 
        if input has shape (8, 6, 4, 2) and dims is (2, 2), then dims is treated as (1, 1, 2, 2).

        Analogously, if input has fewer dimensions than dims specifies, 
        then input is treated as if it were unsqueezed at dimension zero until it has as many dimensions as dims specifies. 
        For example, if input has shape (4, 2) and dims is (3, 3, 2, 2), then input is treated as if it had the shape (1, 1, 4, 2).

        This function is similar to NumPy's tile function.

        input (Tensor) - the tensor whose elements to repeat.
        dims (tuple) - the number of repetitions per dimension.
        ...
        """
        t = torch.arange(0, 10, 1.0)
        u = torch.tile(input=t, dims=(2, 1))
        self.assertEqual(u.shape, (2, 10))
        u = torch.tile(input=t, dims=(5,))
        self.assertEqual(u.shape, (50,))
        u = torch.tile(input=t, dims=(1, 5))
        self.assertEqual(u.shape, (1, 50))
        # fancy dims
        # 0. dims is fewer, then redudant dimensions of tensor is ignored.
        t = torch.arange(0, 64, 1.0).reshape(4, 4, 4)
        u = torch.tile(input=t, dims=(4,))
        self.assertEqual(u.shape, (4, 4, 16))
        # 1. dims is more.
        u = torch.tile(input=t, dims=(4, 4, 4, 4))
        self.assertEqual(u.shape, (4, 16, 16, 16))

    def TestPermute(self):
        """
        Returns a view of the original tensor input with its dimensions permuted.

        input (Tensor) - the input tensor.
        dims (tuple of python:ints) - The desired ordering of dimensions

        permute() and tranpose() are similar. transpose() can only swap two dimension. But permute() can swap all the dimensions.

        Note that, in permute(), you must provide the new order of all the dimensions. In transpose(), you can only provide two dimensions.
        tranpose() can be thought as a special case of permute() method in for 2D tensors.
        """
        t = torch.arange(0, 64, 1.0).reshape(8, 4, 2)
        u = torch.permute(t, dims=(2, 1, 0))
        self.assertEqual(u.shape, (2, 4, 8))

    def TestReshape(self):
        """
        """
        t = torch.arange(0, 10, 1.0).reshape(5, 2)
        u = t.reshape(2, 5)
        v = t.transpose(dim0=1, dim1=0)
        w = t.permute(dims=(1, 0))
        self.assertTrue(t.is_contiguous())
        self.assertTrue(u.is_contiguous())
        self.assertTrue(not v.is_contiguous())
        self.assertTrue(not w.is_contiguous())

    def tearDown(self) -> None:
        return super().tearDown()


def CreateTensorTestSuit():
    suit = unittest.TestSuite()
    suit.addTest(TensorTestCase("TestTensor"))
    suit.addTest(TensorTestCase("TestAsarray"))
    suit.addTest(TensorTestCase("TestAsTensor"))
    suit.addTest(TensorTestCase("TestAsStrided"))
    suit.addTest(TensorTestCase("TestFromNumpy"))
    suit.addTest(TensorTestCase("TestZeros"))
    suit.addTest(TensorTestCase("TestZerosLike"))
    suit.addTest(TensorTestCase("TestOnes"))
    suit.addTest(TensorTestCase("TestOnesLike"))
    suit.addTest(TensorTestCase("TestEmpty"))
    suit.addTest(TensorTestCase("TestEmptyLike"))
    suit.addTest(TensorTestCase("TestFull"))
    suit.addTest(TensorTestCase("TestFullLike"))
    suit.addTest(TensorTestCase("TestEye"))
    suit.addTest(TensorTestCase("TestArange"))
    suit.addTest(TensorTestCase("TestLinspace"))
    suit.addTest(TensorTestCase("TestLogspace"))
    suit.addTest(TensorTestCase("TestSqueeze"))
    suit.addTest(TensorTestCase("TestUnsqueeze"))
    suit.addTest(TensorTestCase("TestUnbind"))
    suit.addTest(TensorTestCase("TestTranspose"))
    suit.addTest(TensorTestCase("TestT"))
    suit.addTest(TensorTestCase("TestStack"))
    suit.addTest(TensorTestCase("TestVstack"))
    suit.addTest(TensorTestCase("TestHstack"))
    suit.addTest(TensorTestCase("TestSplit"))
    suit.addTest(TensorTestCase("TestTensorSplit"))
    suit.addTest(TensorTestCase("TestDsplit"))
    suit.addTest(TensorTestCase("TestHsplit"))
    suit.addTest(TensorTestCase("TestTake"))
    suit.addTest(TensorTestCase("TestComplex"))
    suit.addTest(TensorTestCase("TestAdjont"))
    suit.addTest(TensorTestCase("TestArgwhere"))
    suit.addTest(TensorTestCase("TestCat"))
    suit.addTest(TensorTestCase("TestChunk"))
    suit.addTest(TensorTestCase("TestWhere"))
    suit.addTest(TensorTestCase("TestTile"))
    suit.addTest(TensorTestCase("TestPermute"))
    suit.addTest(TensorTestCase("TestReshape"))
    return suit


if __name__ == "__main__":
    unittest.TextTestRunner().run(CreateTensorTestSuit())
